/*
 * Copyright (c) 2024 Google Inc. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

//! Rust support library for the Trusty kernel

#![no_std]
#![feature(cfg_version)]
// C string literals were stabilized in Rust 1.77
#![cfg_attr(not(version("1.77")), feature(c_str_literals))]
#![deny(unsafe_op_in_unsafe_fn)]
// new_uninit is stable as of Rust 1.82
#![cfg_attr(not(version("1.82")), feature(new_uninit))]
// raw_ref_op is stable as of Rust 1.82
#![cfg_attr(not(version("1.82")), feature(raw_ref_op))]
#![feature(box_as_ptr)]

use alloc::format;
use core::ffi::CStr;
use core::panic::PanicInfo;

mod sys {
    #![allow(clippy::upper_case_acronyms)]
    #![allow(unused)]
    #![allow(non_camel_case_types)]
    #![allow(non_upper_case_globals)]
    #![allow(unsafe_op_in_unsafe_fn)]
    #![allow(clippy::missing_safety_doc)]
    use num_derive::FromPrimitive;
    include!(env!("BINDGEN_INC_FILE"));
}

pub mod err;
pub mod event;
pub mod handle;
pub mod handle_set;
pub mod init;
pub mod interrupt;
pub mod ipc;
pub mod ktipc;
pub mod log;
pub mod macros;
pub mod mmu;
pub mod spinlock;
pub mod sync;
pub mod thread;
pub mod uuid;
pub mod vmm;

pub use sys::paddr_t;
pub use sys::status_t;
pub use sys::uuid_t;
pub use sys::vaddr_t;
pub use sys::Error;

// NOTE: `INFINITE_TIME` is defined in `lk/types.h` as `UINT32_MAX`,
// which in turn is defined as `UINT_MAX`, which is not recognized
// by bindgen according to the bug below so we use `u32::MAX`.
// See <https://github.com/rust-lang/rust-bindgen/issues/1636>.
pub const INFINITE_TIME: u32 = u32::MAX;

#[panic_handler]
fn handle_panic(info: &PanicInfo) -> ! {
    let panic_message = format!("{info}\0");
    let panic_message_c = CStr::from_bytes_with_nul(panic_message.as_bytes())
        .expect("Unexpected null byte in panic message");
    // SAFETY: Calling C function with string pointers that outlive the call
    unsafe { sys::_panic(c"Rust in Trusty kernel %s\n".as_ptr(), panic_message_c.as_ptr()) }
}
